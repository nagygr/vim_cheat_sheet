% Copyright (c) 2016 Gergely Nagy
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
% documentation files (the "Software"), to deal in the Software without restriction, including without
% limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
% Software, and to permit persons to whom the Software is furnished to do so, subject to the following
% conditions:
% 
% The above copyright notice and this permission notice shall be included in all copies or substantial portions
% of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
% TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%\LoadClass[8pt]{article}
\LoadClass[fontsize=6pt]{scrartcl}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cs}[2016/05/02 Cheat sheet document style]

\RequirePackage[paper=a4paper, top=.5cm, left=.8cm, right=.8cm, bottom=.5cm, includeheadfoot, landscape]{geometry}
\RequirePackage{fancyhdr} % must be loaded after geometry!
\RequirePackage[utf8]{inputenc}
\RequirePackage[english]{babel}
\RequirePackage{graphicx}
\RequirePackage[pdfborder={0 0 0}]{hyperref}
\RequirePackage[T1]{fontenc}
\RequirePackage{listings} % for better code listing
\RequirePackage{bold-extra} % needed by listings
\RequirePackage[usenames,dvipsnames]{color}
\RequirePackage[lighttt]{lmodern}
\RequirePackage{hfoldsty} % old style numbers
\RequirePackage{datetime}
\RequirePackage{caption}
\RequirePackage{enumitem}
\RequirePackage{longtable}
\RequirePackage{amssymb} % checkbox (checked: $\boxtimes$ -- unchecked: $\square$)
\RequirePackage{url} % defines the command \path which can be used to typeset long paths and anything long in tt
\RequirePackage{changepage}
\RequirePackage[percent]{overpic}
\RequirePackage{wrapfig}
\RequirePackage{multicol}

\definecolor{darkgreen}{RGB}{0,117,192}
\definecolor{midgreen}{RGB}{0,117,192}
\definecolor{lightgreen}{RGB}{139,219,1}

\setlength{\parindent}{0cm}
\setlength{\columnseprule}{1pt}
\renewcommand{\columnseprulecolor}{\color{darkgreen}}


\newcommand{\colortitle}[1]{\textcolor{darkgreen}{#1}}
\newcommand{\colorsection}[1]{\textcolor{BurntOrange}{#1}}
\newcommand{\colorentry}[1]{\textbf{\textcolor{midgreen}{#1}}}
\newcommand{\colorpagenum}[1]{\textbf{\textcolor{BurntOrange}{#1}}}
\newcommand{\colorcomment}[1]{\tiny{\textit{\textcolor{MidnightBlue}{#1}}}}
\newcommand{\colorbigcomment}[1]{\normalsize{\textit{\textcolor{MidnightBlue}{#1}}}}
\newcommand{\colordate}[1]{\textbf{\textcolor{darkgreen}{#1}}}

\hypersetup{colorlinks, breaklinks, urlcolor=darkgreen, linkcolor=black}

\captionsetup[figure]{labelfont={bf,it},textfont={it}}
\captionsetup[table]{labelfont={bf,it},textfont={it}}

\newcommand{\cstitle}{Cheat sheet}
\newcommand{\cssettitle}[1]{\renewcommand{\cstitle}{#1}}
\newcommand{\cshl}{}
\newcommand{\cssethl}[1]{\renewcommand{\cshl}{#1}}
\newcommand{\cshr}{}
\newcommand{\cssethr}[1]{\renewcommand{\cshr}{#1}}
\newcommand{\csfl}{}
\newcommand{\cssetfl}[1]{\renewcommand{\csfl}{#1}}
\newcommand{\csfr}{\textit{\colordate{\today}}}
\newcommand{\cssetfr}[1]{\renewcommand{\csfr}{#1}}

\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\fancyhf[HL]{\tiny{\textit{\textbf{\cshl}}}}
\fancyhf[HC]{\Large{\textbf{\textsc{\colortitle{\cstitle}}}}}
\fancyhf[HR]{\colortitle{\tiny{\cshr}}}
\fancyhf[FL]{{\colorbigcomment{\csfl}}}
\fancyhf[FC]{\textit{\textbf{\colorpagenum{\thepage}}}}
\fancyhf[FR]{\csfr}

\lstset{basicstyle=\footnotesize\ttfamily\color{nngbluedark}, stringstyle=\ttfamily,
keywordstyle=\bfseries\ttfamily\color{nngorangedark},aboveskip=10pt, commentstyle=\ttfamily\color{darkgray},
belowskip=10pt, tabsize=2, xleftmargin=5pt, captionpos=b} %, 
%literate={á}}1 {é}{{\'e}}1 {ó}{{\'o}}1 {õ}{{\"o}}1 {ú}{{\'u}}1 {û}{{\"u}}1}

\newcommand{\idez}[1]{,,#1''}
\newcommand{\todo}[1]{\textcolor{red}{$[$TODO: #1$]$}}

\newcommand{\cssec}[1]{
	\vspace{.5em}
	\par\textbf{\colorsection{#1}}\par
	\addcontentsline{toc}{section}{#1}
}

\newcommand{\csentry}[1]
{
\par\colorentry{#1}~
}

\newcommand{\cscomment}[1]{
	\vspace{-0.3em}
	\begin{center}
		\begin{minipage}[h]{0.9\columnwidth}
			\colorcomment{#1}
		\end{minipage}
	\end{center}
	\vspace{-0.3em}
}

% enumitem settings:
\setlist{noitemsep}
\setlist[itemize,1]{label=$\triangleright$, leftmargin=1cm}
\setlist[itemize,2]{label=$\star$}
\setlist[enumerate,1]{leftmargin=1cm}
% enumitem settings -- END


