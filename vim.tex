% Copyright (c) 2016 Gergely Nagy
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
% documentation files (the "Software"), to deal in the Software without restriction, including without
% limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
% Software, and to permit persons to whom the Software is furnished to do so, subject to the following
% conditions:
% 
% The above copyright notice and this permission notice shall be included in all copies or substantial portions
% of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
% TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
\documentclass{cs}
\usepackage{lipsum}

\cssettitle{vim cheat sheet}
\cssethl{\href{https://gitlab.com/nagygr/vim_cheat_sheet}{https://gitlab.com/nagygr/vim\_cheat\_sheet}}
\cssethr{\textit{Gergely Nagy}}
\cssetfl{\textbf{Symbols used:} <>: obligatory arg.~~~~$\blacksquare$~~~~[]: optional arg.~~~~$\blacksquare$~~~~|:
or~~~~$\blacksquare$~~~~$\spadesuit$: needs extension}

\begin{document}
\sloppy

\begin{multicols*}{4}
\cssec{Appearance, basic settings}
\csentry{:set cursorline}inverts current line
\csentry{:colorscheme <scheme>}choose scheme from \texttt{.vim/colors}
\csentry{:set [no]wrap}turns line breaking on/off
\csentry{:set textwidth|tw=<num>}set width for wrap
\csentry{:set smartindent}indent new line smartly
\csentry{:set showmatch}show matching braces
\csentry{:set mouse=a}intelligent mouse handling
\csentry{:set number}turn line numbering on
\csentry{:set nobackup}no backup files
\csentry{:set showcmd}shows commands while typing
\csentry{:set ttyfast}smoother redraws
\csentry{:set laststatus=2}always display statusline
\csentry{:set showmode}show current editing status
\csentry{:set backspace=indent,eol,start}backspace can delete every whitespace
\csentry{:set linebreak}don't break words
\csentry{:set wildmenu}turns wildmenu on
\cscomment{Wildmenu shows completion possibilities for paths \& commands}
\csentry{:set wildmode=longest:full}1$^{st}$ tab completes to longest string \& show possibilites
\csentry{:set filetype=|ft=<ft>}set the type used for syn.hl.
\csentry{:set fileencoding=<e>}set encoding (\emph{utf-8, ucs-2})

\cssec{Buffers}
\csentry{:buffers}show open buffers with numbers
\csentry{bn}jump to next buffer
\csentry{bp}jump to previous buffer
\csentry{:bufdo <cmd>}apply command to every buffer

\cssec{Command line}
\csentry{ctrl-v <spec.char>}enter special char in command line
\csentry{ctrl-r, ctrl-w}copy current word to command line
\csentry{:!<cmd>}execute external (OS) command
\csentry{:r !<cmd>}execute cmd \& write its output to curr. buffer
\csentry{:sh}open shell (return to editing w/ ctrl-d)
\csentry{:pwd}current working directory
\csentry{q:}open command window with history (exit: ctrl-cc)
\csentry{q/}open search window (exit: ctrl-cc)

\cssec{Cscope~$\spadesuit$}
\csentry{cs add <cscope db>}load cscope file
\cscomment{
Create cscope db: cscope -R -b\\
By default only C files are parsed\\
For C++/Java: cscope.files with list of files
}
\csentry{:[tab] cs find <cmd> <word>}execute cmd w/ word
\cscomment{
\textbf{Commands:}\\
-- \textbf{s} (symbol): find all references to a symbol\\
-- \textbf{g} (global): find global definitions\\
-- \textbf{c} (calls): find calls to a function\\
-- \textbf{t} (text): find all instances as text\\
-- \textbf{e} (egrep): egrep search for the word\\
-- \textbf{f} (file): open the given file\\
-- \textbf{i} (includes): find files that include this file\\
-- \textbf{d} (called): find function that this function calls\\
\vspace{.1em}\\
\textbf{tab} result is opened in a new tab
}

\cssec{Ctags~$\spadesuit$}
\csentry{:[tab] tag <type, variable name>} find hits
\cscomment{
Create tags db: ctags -R .\\
}

\cssec{Diff tool}
\csentry{vim -d <files>}start vim in diff mode
\csentry{vimdiff <files>}start vim in diff mode
\csentry{:difft[his]}turn current tab into diff page
\cscomment{The docs should be in splits \& apply difft to both sides}
\csentry{do}obtain changes from the other window
\csentry{dp}put changes to the other window
\csentry{]c}jump to next change
\csentry{[c}jump to previous change
\csentry{:diffupdate}update diff with recent edits

\cssec{Editing}
\cscomment{Most edit commands can be preceded by numbers for repetition or superceded by movement commands}
\csentry{i}start editing at next position
\csentry{I}start editing at the beginning of line
\csentry{A}start editing at the end of line
\csentry{[:]u}undo changes
\csentry{ctrl-r}redo changes
\csentry{y}yank (copy) selection
\csentry{yw}yank word starting at cursor
\csentry{yy}yank current line
\csentry{p}paste yanked text from current position
\csentry{P}paste before current position
\csentry{yiw}yank word under cursor
\csentry{d}delete selection
\csentry{dd}delete current line
\csentry{ddp}swap current line with the one below
\csentry{dw}delete words
\csentry{diw}delete word under cursor
\csentry{D}delete line from cursor
\csentry{x}delete a character
\csentry{xp}swap current character with next
\cscomment{Deleted parts are yanked}
\csentry{cw}overwrite words
\csentry{ciw}overwrite word under cursor
\csentry{C}overwrite line from cursor
\csentry{:changes}recent changes made in vim

\cssec{Folds}
\csentry{:set [no]foldenable}enable automatic folds
\csentry{:set foldmethod=manual}create folds manually
\csentry{zf<movement>}close fold
\csentry{zo}open fold
\csentry{set foldnestmax=<num>}max. nesting
\csentry{set foldlevel=<num>}level of folding

\cssec{Macros}
\csentry{q<reg.name><cmds>q}record macro in register
\csentry{@<reg.name|@>}execute macro (@: repeat latest)

\cssec{Marks}
\cscomment{Lowercase letter for bookmarks in current file, uppercase letters remember file name also.}
\csentry{m<letter>}save bookmark at current position
\csentry{`<letter>}jump to bookmark
\cscomment{\textbf{Special marks:}\\--~\textbf{`\textit{n}}: jump to $n^{th}$ file before this\\--~\textbf{``}: position
before last jump\\--~\textbf{`.}: jump to last change in file}
\csentry{d\,|\,y\,|\,c<ltr.>}delete|yank|change (from curr.pos to mark)
\csentry{:marks [letters]}list of current [given] marks

\cssec{Moving around}
\csentry{h|j|k|l}:$\leftarrow$|$\downarrow$|$\uparrow$|$\rightarrow$
\csentry{zt|zz|zb}move current line to top|center|bottom
\csentry{:H|M|L}move cursor to top|center|bottom
\csentry{ctrl-o|``}move to the previous position in a file
\csentry{`.}move to latest edit position
\csentry{ctrl-u|d}scroll half page up|down
\csentry{:set scrollbind|scb}binds splits if issued on both sides
\csentry{:set !scb}inverts scrollbind setting

\cssec{Opening, finding files}
\csentry{:enew}open empty \& unnamed file
\csentry{tabe **/<filename>}search recursively \& open file
\csentry{\scriptsize [vim]grep[!]/<pattern>/[gj] <file(s)>}built-in grep
\cscomment{
--~\textbf{g}: every match is returned (not one/line)\\
--~\textbf{j}: vim won't jumpt to 1$^{st}$ match\\
--~<files> can be given as **/<pattern>\\
--~results gathered in quickfix menu
}
\csentry{ctrl-w gf}open file/path under cursor in new tab
\csentry{ctrl-w gF}same \& jump to line (\verb+path:line_no+)

\cssec{Paths and filenames}
\csentry{\%}current file's name (variable)
\csentry{\%:p:h}\textit{p}: full path, \textit{h}: head (w/o filename)
\csentry{\%:r}filename w/o extension (:tabe \%:r.cpp)
\csentry{:E}open the directory of the current file for browsing
\csentry{:cd <path>}change working directory
\csentry{:lcd <path>}change working directory in current tab
\csentry{:set path+=<path>}extends search path
\csentry{:f}prints info on current file to command line

\cssec{Quickfix menu}
\cscomment{
List of compile errors, grep results.\\
Shared between every tab/buffer.
}
\csentry{:cn}show next error
\csentry{:cw|:copen} open quickfix menu
\csentry{:cl}list of the quickfix results
\csentry{:[num]cc}open n$^{th}$ result (default: 1$^{st}$)

\cssec{Saving, exiting}
\csentry{:q}exit
\csentry{:q!}exit even if there are unsaved documents
\csentry{:w [file name]}save a file
\csentry{:wq}save and exit
\csentry{:sav <file name>}"save as" and open
\csentry{:qall\,|\,:qa}close every tab and exit
\csentry{:wqall\,|\,:wqa}save+close every tab and exit
\csentry{ZZ} save and close current file

\cssec{Search and replace}
\csentry{?<text>}search upwards
\csentry{/<text>}search downwards
\csentry{\#\,|\,*}previous|next instance of word under cursor
\csentry{\%}jump to matching brace
\csentry{[\{~|~]\}}jump to beginning|end of current code block
\csentry{[(~|~])}jump to beginning|end of parenthesis
\csentry{[interval]s/<regex1>/<regex2>/<args>}
\cscomment{\textbf{Interval}:\\
--~$\emptyset$: current line\\
--~\textbf{n,m}: $n^{th}\rightarrow m^{th}$ line\\
--~\textbf{\%}: the entire text\\
\textbf{Regex}:\\
--~\textit{regex1}: search pattern\\
--~\textit{regex2}: replace pattern\\
\textbf{Args}:\\
--~\textbf{g}: every match in a line (default:1$^{st}$)\\
--~\textbf{c}: confirm every change
}
\csentry{:set hls[earch]}highlights search matches
\csentry{:set nohls[earch]}switches highlighting off
\csentry{:noh}hides highlighted matches
\csentry{:set ic|noic}set|reset ignore case mode
\csentry{:g[!]/<pattern>[/<command>]}
\cscomment{
Apply command to lines matching pattern \\
--~\textbf{!}: apply cmd to lines that \emph{don't} match \\
--~\textbf{:v} = :g! \\
--~\textbf{no cmd}: only show lines that match \\
--~\textbf{command}: e.g. d (delete) \\
--~\textbf{:g/<pattern>/normal <cmd>}: applies any vim command to matching lines
}

\cssec{Selection}
\cscomment{Commands can be applied to active selections}
\csentry{v}select at character level
\csentry{V}select lines
\csentry{ctrl-v}select a block (arbitrary rectangle)

\cssec{Sessions}
\csentry{:mksession <session>}save settings\, \& \,tabs
\csentry{:source <session>}source a session file
\csentry{vim -S <session>}start vim with session file

\cssec{Spell checking \& syntax highlighting}
\csentry{:set spell spelllang=en\_us}turn spell check on
\cscomment{Vim offers to download missing lang.files ($\sim$/.vim/spell)}
\csentry{:set nospell}switch spell check off
\csentry{[s}jump to previous spelling error
\csentry{]s}jump to next spelling error
\csentry{zg}add word under cursor to custom dictionary
\csentry{zG}add word under cursor to temp. dictionary
\csentry{z=}give advice for word under cursor
\csentry{:syntax on|off}turns syntax highlighting on|off
\csentry{:set syntax=<language>}forces syntax highlighting

\cssec{Splits}
\csentry{:new}open empty \& unnamed file in horizontal split
\csentry{:vnew}open empty \& unnamed file in vertical split
\csentry{ctrl-ws}split windows horizontally
\csentry{ctrl-wv}split windows vertically
\csentry{ctrl-ww}move between splits
\csentry{ctrl-wp}switch to the previous window
\csentry{ctrl-w$\leftarrow$|$\rightarrow$|$\uparrow$|$\downarrow$}move cursor to split $\leftarrow$|$\rightarrow$|$\uparrow$|$\downarrow$
\csentry{ctrl-wx}Exchange two splits
\csentry{ctrl-wr}Rotate splits
\csentry{ctrl-wq}quit a split (=:q)
\csentry{ctrl-w=}equalize split sizes
\csentry{[\textit{n}]ctrl-w+}increase lines in split [by $n$]
\csentry{[\textit{n}]ctrl-w-}decrease line in split [by $n$]
\csentry{[\textit{n}]ctrl-w>}increase columns in split [by $n$]
\csentry{[\textit{n}]ctrl-w<}decrease columns in split [by $n$]
\csentry{:windo <cmd>}apply cmd to every split
\csentry{ctrl-w H}current split to left in full height
\csentry{ctrl-w K}current split to top in full width
\csentry{:Tabmerge\,[n][m]$\spadesuit$} turn tabs$\rightarrow$splits (def:curr.w/right)

\cssec{Starting vim}
\csentry{vim -p <files>}Open files in tab pages
\csentry{vim -O <files>}Open files in vertical splits
\csentry{vim +<ln num> <file>}Open file at line
\csentry{vim +/<func name> <file>}Open file at function

\cssec{Tab pages}
\csentry{:tabnew}open empty \& unnamed file in tab
\csentry{:tabe[dit] <file>}open file in new tab page
\csentry{:tabe +<ln num> [**/]<file>}Open file at line
\csentry{:tabe +/<func name> <file>}Open file at function
\csentry{gt\,|\,gT}move to previous\,|\,next tab page
\csentry{\textit{n}gt}jump to $n^{th}$ tab page (1$^{st}$ is 1)
\csentry{:tabm \textit{n}}move this tab after $n^{th}$ (0 $\rightarrow$1$^{st}$)
\csentry{:tabm $+$|$-$\textit{n}}move this tab \textit{n} right\,|\,left
\csentry{:tabfir[st]\,|\,tabl[ast]} jump to first\,|\,last tab
\csentry{:tabs}show open tabs
\csentry{:tab sball}turn open buffers into tab pages
\csentry{:tabdo <cmd>}apply cmd to every tab page
\csentry{:set tabpagemax=<num>}max. number of tabpages

\cssec{Tabs and whitespaces}
\csentry{:set tabstop=<num>}size of a tab in spaces
\csentry{:set sw=<num>}indentation size
\csentry{:set autoindent}turn autoindent on
\csentry{>>\,|\,<<}move current line or selection with one tab to left|right
\csentry{:set list|nolist}show whitespaces on|off
\cscomment{
The shown whitespace characters set w/ lcs:\\
:set lcs=tab:>-,trail:-
}

\cssec{Text formatting, completion}
\csentry{gq}format (selected) text to textwidth
\csentry{=}indent (selected) code
\csentry{ctrl-n}complete word (based on open docs)
\csentry{ctrl-x, ctrl-f}complete file path (search from current dir)
\csentry{ctrl-x, ctrl-l}complete line
\csentry{ctrl-x, ctrl-o}omni completion
\csentry{:[range]sort[!][u]}sort lines
\cscomment{-- \textbf{range}: line range\\-- \textbf{!}: reverse order\\-- \textbf{u}: remove duplicates}

\cssec{Variables (registers)}
\cscomment{Variable names are the characters \& digits}
\csentry{:reg [name]}show variable value (default: every variable)
\csentry{"<variable>}dereference variable in command mode
\csentry{"<var><cmd.>}use variable as in/out for command
\csentry{ctrl-r <var.>}dereference variable in command line
\csentry{let @<var.>=<expr>}assign variable in command
\csentry{"+p}yank OS clipboard from current position
\csentry{:let @+=expand("\%:p")}copy curr.file path to clipbrd
\csentry{:let @+="<ctrl-R>\%"}copy current relative file path $\rightarrow$ clipboard
\csentry{:put=getcwd()}write current working dir to current doc

%\todo{regex}

\end{multicols*}
\end{document}
